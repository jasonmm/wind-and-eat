var colors = ['red', 'green', 'blue'];
var foodSize = 5;
var food = null;
var canvasElem = document.getElementById('world');
var canvas = canvasElem.getContext('2d');
var windVector = {
    dx: rand(-3, 3),
    dy: rand(-3, 3)
};
var windMagnitude = Math.round(Math.sqrt(windVector.dx * windVector.dx + windVector.dy * windVector.dy));


initialize();

var creatures = createCreatures();

// animationFrame = window.requestAnimationFrame(mainLoop)
// sleep(3000).then(() => {window.cancelAnimationFrame(animationFrame)});

var mainLoopInterval = setInterval(mainLoop, 100);
var windInterval = setInterval(updateWind, 2000);

setTimeout(clearIntervals, 60000);

function mainLoop() {
    creatures = move();

    var foodCreatures = creaturesOnFood();
    if (foodCreatures.length > 0) {
        food = null;
        creatures = creatures.map(function(c) {
            if (foodCreatures.indexOf(c.color) > -1) {
                c.size++;
            }
            return c;
        });
    }
    placeFood();

    creatures = creatures.map(function(c) {
        var collidedCreature = getCollidedWith(c);
        if (collidedCreature !== null) {
            //console.log([c.color, collidedCreature.color]);
            c.size -= Math.floor(collidedCreature.size / 2);
            if (c.size < 0) {
                c.size = 0;
            }
        }
        return c;
    });

    document.getElementById("wind").innerText = windVectorToText() + " at strength " + windMagnitude;

    canvas.clearRect(0, 0, canvasElem.width, canvasElem.height);
    draw();
    // animationFrame = window.requestAnimationFrame(mainLoop);
}

function clearIntervals() {
    clearInterval(mainLoopInterval);
    clearInterval(windInterval);
}

/**
 * Returns the creature with the given `color`.
 */
function getByColor(color) {
    return creatures.filter(function(c) {
        return c.color === color;
    }).pop();
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

/**
 * Returns a text representation of the wind vector's direction.
 */
function windVectorToText() {
    var windX = Math.abs(windVector.dx) / (windVector.dx === 0 ? 1 : windVector.dx);
    var windY = Math.abs(windVector.dy) / (windVector.dy === 0 ? 1 : windVector.dy);
    var ret = [
        [0, 0, "no wind"],
        [0, 1, "S"],
        [0, -1, "N"],
        [1, 0, "E"],
        [1, 1, "SE"],
        [1, -1, "NE"],
        [-1, 0, "W"],
        [-1, 1, "SW"],
        [-1, -1, "NW"]
    ].filter(function(d) {
        return d[0] === windX && d[1] === windY;
    });

    return ret[0][2];
}

/**
 * Updates the wind vector and magnitude.
 */
function updateWind() {
    windVector = {
        dx: rand(-3, 3),
        dy: rand(-3, 3)
    };
    windMagnitude = Math.round(Math.sqrt(windVector.dx * windVector.dx + windVector.dy * windVector.dy));
}

/**
 * Initializes the app.
 */
function initialize() {
    resizeCanvas();
    placeFood();
}

/**
 * If there is no food in the world then place the food randomly.
 */
function placeFood() {
    if (food === null) {
        food = randomCoord();
    }
}

/**
 * Returns the size of the page. Used for correctly sizing the canvas.
 */
function viewportSize() {
    var e = document.documentElement;
    var b = document.getElementsByTagName('body')[0];
    var x = window.innerWidth || e.clientWidth || b.clientWidth;
    var y = window.innerHeight || e.clientHeight || b.clientHeight;
    return {
        x: x,
        y: y
    };
}

/**
 * Resizes the canvas to fit the size of the browser.
 */
function resizeCanvas() {
    var viewport = viewportSize();
    var c = document.getElementById('world');
    c.width = 300; //viewport.x - 30;
    c.height = 300; //viewport.y - 30;
}

/**
 * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/random#Getting_a_random_number_between_two_values
 */
function rand(min, max) {
    return Math.random() * (max - min) + min;
}

function randomCoord() {
    var c = document.getElementById('world');
    return {
        x: rand(0, c.width),
        y: rand(0, c.height)
    };
}

/**
 * Returns an array of maps describing creatures. The returned array has the
 * same number of elements as the COLORS array.
 */
function createCreatures() {
    return colors.map(function(color) {
        return {
            color: color,
            viewport: 1,
            speed: 1,
            size: 1,
            direction: {
                x: 1,
                y: 1
            },
            position: randomCoord()
        };
    });
}

function degToRad(deg) {
    return deg * Math.PI / 180;
}

/**
 * Returns the distance between creatures `c1` and `c2`.
 */
function distanceBetween(c1, c2) {
    var dx = c1.position.x - c2.position.x;
    var dy = c1.position.y - c2.position.y;
    return Math.sqrt(dx * dx + dy * dy);
}

/**
 * Returns an array of the colors of the creatures that overlap the food.
 */
function creaturesOnFood() {
    return creatures
        .map(function(c) {
            return Math.abs(c.position.x - food.x) < 4 && Math.abs(c.position.y - food.y) < 4 ? c.color : false;
        })
        .filter(function(x) {
            return x !== false;
        });
}

/**
 * Returns the first creature found that has collided with creature `c`.
 */
function getCollidedWith(c) {
    for (var c2 of creatures) {
        if (c.color !== c2.color) {
            if (distanceBetween(c, c2) <= c.size + c2.size) {
                return c2;
            }
        }
    }

    return null;
}

/**
 * Returns an array of creatures moved to a new location in the world.
 */
function move() {
    return creatures.map(function(c) {
        // Creatures of size 0 do not move.
        if (c.size === 0) {
            return c;
        }

        // The vector the creature needs to take to reach the food.
        var dx = food.x - c.position.x;
        var dy = food.y - c.position.y;
        var foodAngle = Math.atan(dy / dx);
        var foodMagnitude = windMagnitude + c.size; // (1 / c.size) * (canvasElem.width/3);
        var foodVector = {
            dx: foodMagnitude * Math.cos(foodAngle),
            dy: foodMagnitude * Math.sin(foodAngle)
        };

        // The creature's actual vector is a combination of the direction they want
        // to take to the food and direction the wind is pushing them.
        var creatureVector = {
            dx: windVector.dx + foodVector.dx,
            dy: windVector.dy + foodVector.dy
        };
        var newX = c.position.x + creatureVector.dx;
        if (newX > canvasElem.width) {
            newX = 0;
        }
        if (newX < 0) {
            newX = canvasElem.width;
        }
        var newY = c.position.y + creatureVector.dy;
        if (newY > canvasElem.height) {
            newY = 0;
        }
        if (newY < 0) {
            newY = canvasElem.height;
        }

        return Object.assign({}, c, {
            position: {
                x: newX,
                y: newY
            }
        });
    });
}

/**
 * Draws a frame after all movement.
 */
function draw() {
    drawCreatures(creatures);
    drawFood(food);
}

function drawCreatures() {
    for (var creature of creatures) {
        canvas.beginPath();
        canvas.fillStyle = creature.color;
        canvas.arc(creature.position.x, creature.position.y, creature.size, 0, 360);
        canvas.fill();
    }
}

function drawFood() {
    if (food !== null) {
        canvas.fillStyle = 'grey';
        canvas.fillRect(food.x, food.y, foodSize, foodSize);
    }
}
